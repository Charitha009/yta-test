import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FizzBuzzCalculateService {

  constructor() { }

  calculateMultiplesOfOne(max: number, multiple: number): number {
    let count = 0;
    for (let i = 0; i <= max; i++) {
      if (i % multiple === 0) {
        count++
      }
    }
    return count;
  }

  calculateMultiplesOfTwo(max: number, multiple1: number, multiple2: number): number {
    let count = 0;
    for (let i = 0; i <= max; i++) {
      if (i % multiple1 === 0 && i % multiple2 === 0) {
        count++
      }
    }
    return count;
  }

  validateInput(input: string): boolean {
    const commands = input.trim().split(' ');
    if(commands.length === 2) {
      if (commands[0].toLowerCase() === 'fizz' || commands[0].toLowerCase() === 'buzz' || commands[0].toLowerCase() === 'fizzbuzz') {
        return (!isNaN(+commands[1])) && +commands[1] <= 100 && +commands[1] >= 0;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
