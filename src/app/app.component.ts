import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject} from "rxjs";
import {UiService} from "./services/ui.service";
import {FizzBuzzCalculateService} from "./services/fizz-buzz-calculate.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  resultLine1$ = new BehaviorSubject('');
  resultLine2$ = new BehaviorSubject('');
  resultLine3$ = new BehaviorSubject('');
  resultLine4$ = new BehaviorSubject('');

  commandForm =  new FormGroup({
    command: new FormControl(null, { validators: [Validators.required]})
  });

  constructor(
    private uiService: UiService,
    private fizzBuzzCalculateService: FizzBuzzCalculateService
  ) {}

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.initResult();
    const currentDateTime = new Date().getMilliseconds();
    if (this.commandForm.valid) {
      const userInput = this.commandForm.value.command;
      if(this.fizzBuzzCalculateService.validateInput(userInput)) {
        const commands = userInput.trim().split(' ');
        if (commands[0] === 'fizz') {
          this.resultLine1$.next('Command received: fizz ' + commands[1]);
          this.resultLine2$.next('Calculating fizz ' + commands[1] + '...');
          this.resultLine3$.next('Result: ' + this.fizzBuzzCalculateService.calculateMultiplesOfOne(+commands[1], 3));
          this.resultLine4$.next('Time: ' + (new Date().getMilliseconds() - currentDateTime) + 'ms');
        } else if (commands[0] === 'buzz') {
          this.resultLine1$.next('Command received: buzz ' + commands[1]);
          this.resultLine2$.next('Calculating buzz ' + commands[1] + '...');
          this.resultLine3$.next('Result: ' + this.fizzBuzzCalculateService.calculateMultiplesOfOne(+commands[1], 5));
          this.resultLine4$.next('Time: ' + (new Date().getMilliseconds() - currentDateTime) + 'ms');
        } else { //fizzbuzz
          this.resultLine1$.next('Command received: fizzbuzz ' + commands[1]);
          this.resultLine2$.next('Calculating fizzbuzz ' + commands[1] + '...');
          this.resultLine3$.next('Result: ' + this.fizzBuzzCalculateService.calculateMultiplesOfTwo(+commands[1], 3, 5));
          this.resultLine4$.next('Time: ' + (new Date().getMilliseconds() - currentDateTime) + 'ms');
        }
      } else {
        this.uiService.showSnackbarMessage('Please enter valid command');
      }
    } else {
      this.uiService.showSnackbarMessage('Please enter a command');
    }
  }

  initResult(): void {
    this.resultLine1$.next('');
    this.resultLine2$.next('');
    this.resultLine3$.next('');
    this.resultLine4$.next('');
  }
}
